import random
import names
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from urllib3.connectionpool import xrange

from .models import Post, Category, Author
from .serializers import PostSerializer
from random_words import RandomWords


class AllPostView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def list(self, request):
        queryset = self.get_queryset()
        serializer = PostSerializer(queryset, many=True)
        return Response({'posts': serializer.data})


class CategoriesPostView(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        category = get_object_or_404(Category, pk=self.kwargs['pk'])
        queryset = Post.objects.filter(category=category)
        return queryset

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = PostSerializer(queryset, many=True)
        return Response({'category_posts': serializer.data})


class AuthorsPostView(generics.ListAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        author = get_object_or_404(Author, pk=self.kwargs['pk'])
        queryset = Post.objects.filter(author=author)
        return queryset

    def list(self, request, **kwargs):
        queryset = self.get_queryset()
        serializer = PostSerializer(queryset, many=True)
        return Response({'author_posts': serializer.data})


class SinglePostView(generics.RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({'post': serializer.data})


class CreatePostView(generics.ListCreateAPIView):
    serializer_class = PostSerializer

    def get_queryset(self):
        queryset = Post.objects.all()
        category_exists = Category.objects.all().exists()
        if not category_exists:
            r = RandomWords()
            categories = r.get_random_words(minLength=5, maxLength=10, limit=20)
            for i in xrange(20):
                Category.objects.create(name=categories[i])
        author_exists = Author.objects.all().exists()
        if not author_exists:
            for _ in xrange(10):
                random_author = names.get_first_name()
                Author.objects.create(name=random_author)
        post_exists = Post.objects.all().exists()
        if not post_exists:
            post = Post.objects.create(title=f'Title#', text='lorem ipsum',
                                       author=random.choice(Author.objects.all()))
            post.category.set([random.choice(Category.objects.all()),
                               random.choice(Category.objects.all()),
                               random.choice(Category.objects.all())])
        else:
            a = Post.objects.all().last().pk
            post = Post.objects.create(title=f'Title#{a}', text='lorem ipsum',
                                       author=random.choice(Author.objects.all()))
            post.category.set([random.choice(Category.objects.all()),
                               random.choice(Category.objects.all()),
                               random.choice(Category.objects.all())])
        return queryset

    def list(self, request):
        queryset = self.get_queryset()
        serializer = PostSerializer(queryset, many=True)
        return Response({'posts': serializer.data})
